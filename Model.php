<?php
abstract class Model{
  //Declaramos variables globales
  protected $user;
  protected $pass;
  protected $db;
  protected $host;
  protected $port;
  protected $conn;
  protected static $all;
  protected $table;
  private $conditions;
  private $selectors;
  private $orders;
  private $operators;
  //Constructor donde asignamos el valor correspondiente a cada variable
  function __construct(){
    $this->user="root";
    $this->pass="";
    $this->db="libreria";
    $this->host="127.0.0.1";
    $this->port=3306;
    $this->conditions = [];
    $this->operators = ['=','<>','!=','in','not in','>=','<='];
  }
  //Funcion donde traemos todas las filas de la tabla
  public function all(){
    return $this->executeQuery("select * from ".$this->table);
  }
  public function get(){
    $query = "SELECT ";
    if(!empty($this->selectors)){
      foreach ($this->selectors as $value) {
        $query.="$value, ";
      }
      $query = trim($query,',');
    }else {
      $query.="* ";
    }
    $query .= " FROM {$this->table}";
    if(!empty($this->conditions)){
      $query .= " WHERE ";
      foreach ($this->conditions as $value) {
        $query .= "{$value} ";
      }
      $query = trim($query,'AND ');
      $query = trim($query,'OR ');
    }
    echo $query;
    return $this->executeQuery($query);
  }
  //Funcion donde traemos las filas con el campo que corresponde al valor dado
  public function where($field,$op,$value=null){
    if($value){
      if(in_array($op,$this->operators)){
        array_push($this->conditions,"{$field} {$op} '{$value}' AND");
      }else{
        throw new Exception("Ingreso un operador no valido.");
        return;
      }
    }else{
    array_push($this->conditions,"{$field} = '{$op}' AND");
    }
    return $this;
  }
  public function orWhere($field,$op,$value=null){
    if($value){
      if(in_array($this->operators,$op)){
        array_push($this->conditions,"{$field} {$op} '{$value}' OR");
      }else{
        throw new Exception("Ingreso un operador no valido.");
      }
    }else{
    array_push($this->conditions,"{$field} = '{$op}' OR");
    }
    return $this;
  }
  //Funcion donde ejecutamos la sentencia SQL formada por otra funcion
  public function executeQuery($query){
    //Se abre la conexion;
    $this->openConnection();
    //Se obtiene el resultset de la consulta
    $result = $this->conn->query($query);
    //Obtenenos todas las filas y las ponemos en data
    $data = [];
    while($row = $result->fetch_assoc()){ array_push($data,$row); }
    //Cerramos la conexion
    $this->closeConection();
    //Regresamos los datos
    return $data;
  }
  //Abrimos la conexion
  public function openConnection(){
    $this->conn = new mysqli($this->host, $this->user, $this->pass, $this->db,$this->port);
    $this->conn->set_charset('utf8');
  }
  //Cerramos la conexion
  public function closeConection(){
    mysqli_close($this->conn);
  }
}
